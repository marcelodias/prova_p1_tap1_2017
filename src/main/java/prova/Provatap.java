package prova;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import helper.JsonHelper;
import model.Veiculo;

@WebServlet(urlPatterns = "/prova-1704")

public class Provatap  extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private List<Veiculo> lista = new ArrayList<>();
	private JsonHelper jsonHelper = new JsonHelper();
	
	//post
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//criando e fazendo requerimento
		int id = lista.size() + 1; //id do veiculo come�ando com 1.
		String placa = req.getParameter("placa");
		String nome = req.getParameter("nome");
		String marca = req.getParameter("marca");
		double valor = Double.parseDouble(req.getParameter("valor"));
		
		Veiculo veic = new Veiculo(id, placa, nome, marca, valor);
		lista.add(veic);
		
		//resposta em json
		try {
			resp.getWriter().println(jsonHelper.gerarJson(veic));

		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//delete
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//requisi�ao id
		int id = Integer.parseInt(req.getParameter("id"));
		//procura o id dentro da lista
		for (int i = 0; i < lista.size(); i++) {
			if (lista.get(i).getId() == id) {
				lista.remove(i);
			}
		}
		//teste lembrar de tirar
		resp.getWriter().println("Removido");
	}
	
	//alterar
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//criando variaveis para receber todos ou apenas o que deseja alterar
		
		int id = Integer.parseInt(req.getParameter("id"));
		String placa = req.getParameter("placa");
		String nome = req.getParameter("nome");
		String marca = req.getParameter("marca");
		String valor = req.getParameter("valor");
		
		
		for (int i = 0; i < lista.size(); i++) {//percorrer a lista
			
			if(lista.get(i).getId() == id){ //caso for igual o id da lista com o que foi passado altera
				if(placa != null && placa !=""){
					lista.get(i).setPlaca(placa);
				}
				if(nome != null && nome !=""){
					lista.get(i).setNome(nome);
				}
				if(marca != null && marca !=""){
					lista.get(i).setMarca(marca);
				}
				
				if(valor != null && valor !=""){
					Double auxv = Double.parseDouble(valor);
					lista.get(i).setValor(auxv);
				}			
				
			}
			resp.getWriter().println("[{id:"+lista.get(i).getId()+",marca:"+lista.get(i).getMarca()+",nome:"+lista.get(i).getNome()+",placa:"+lista.get(i).getPlaca()+",valor:"+lista.get(i).getValor()+"");}
		
		
		
	}
	
	//get
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String opcao = req.getParameter("opcao");
		
		if (opcao.equals("buscar placa")) {
			//por placa
			String placa = req.getParameter("placa");
			for (int i = 0; i < lista.size(); i++) {
				if(lista.get(i).getPlaca().equals(placa)){
					resp.getWriter().println("[{id:"+lista.get(i).getId()+",marca:"+lista.get(i).getMarca()+",nome:"+lista.get(i).getNome()+",placa:"+lista.get(i).getPlaca()+",valor:"+lista.get(i).getValor()+"");
				}
			}
			
		}else if (opcao.equals("buscar todos")){
			//mostrar todos
			try {
				
				resp.getWriter().print(jsonHelper.gerarJsonLista(lista));
			} catch (IllegalArgumentException e) {

				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else if(opcao.equals("buscar qtd")){
			//quantidade
			resp.getWriter().println("{qtd:"+lista.size()+"}");
			
		}else if(opcao.equals("buscar mais barato")){
		//mais barato
			int aux = 0;
			for (int i = 0; i < lista.size(); i++) {
				if(lista.get(i).getValor() < lista.get(aux).getValor()){
					aux = i;
				}
			}
			resp.getWriter().println("[{id:"+lista.get(aux).getId()+",marca:"+lista.get(aux).getMarca()+",nome:"+lista.get(aux).getNome()+",placa:"+lista.get(aux).getPlaca()+",valor:"+lista.get(aux).getValor()+"");
			
		}else if(opcao.equals("buscar mais caro")){			
			//buscar mais caro
			int aux = 0;
			for (int i = 0; i < lista.size(); i++) {
				if(lista.get(i).getValor() > lista.get(aux).getValor()){
					aux = i;
				}
			}
			resp.getWriter().println("[{id:"+lista.get(aux).getId()+",marca:"+lista.get(aux).getMarca()+",nome:"+lista.get(aux).getNome()+",placa:"+lista.get(aux).getPlaca()+",valor:"+lista.get(aux).getValor()+"");
			
		}
	}
}
	
